---
layout: handbook-page-toc
title: Manager Onboarding
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Training

### Diversity, Inclusion, and Belonging Training
[Diversity, Inclusion & Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion) is one of Gitlab's 6 values. It's what we believe distinguishes us from other companies. Engineering Managers should take [DIB Training](https://gitlab.com/gitlab-com/diversity-and-inclusion/-/issues/new?issuable_template=diversity-inclusion-belonging-training-template) which reviews key sections of our handbook and a learning path. We all need to work on making sure we are inclusive as we work together to build some really great things.

### Iteration Training
Besides normal onboarding training with various tools and processes.  Engineering Managers should also take the [Iteration](https://about.gitlab.com/handbook/product/product-principles/#iteration) Training.  Engineering Managers can start the process of training by following the steps in this [template](https://gitlab.com/gitlab-com/Product/-/issues/new?issuable_template=iteration-training).  Iteration is a important value of GitLab and not easily learned.  Working with other Engineering Managers and Product Managers during this exercise is encouraged to further enhance the experience.  
